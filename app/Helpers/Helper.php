<?php
function currency()
{
    $arguments = func_get_args();
    $currencyCode = $arguments[1] == 'IRT' ? ' تومان' : '$';
    return number_format($arguments[0]) . $currencyCode;
}
