<?php

return [
    'common' => [
        'no-result-found' => 'هیچ نتیجه ای پیدا نشد',
        'country' => 'کشور',
        'state' => 'محله',
        'true' => 'درست',
        'false' => 'غلط'
    ],

    'layouts' => [
        'my-account' => 'حساب کاربری من',
        'logout' => 'خروج از حساب کاربری',
        'visit-shop' => 'مشاهده فروشگاه',
        'dashboard' => 'داشبورد',
        'sales' => 'سفارشات',
        'orders' => 'سفارشات',
        'shipments' => 'حمل و نقل',
        'invoices' => 'فاکتورها',
        'catalog' => 'کاتالوگ',
        'products' => 'محصولات',
        'categories' => 'دسته بندی ها',
        'attributes' => 'ویژگی ها',
        'attribute-families' => 'خانواده ویژگی ها',
        'customers' => 'مشتریان',
        'groups' => 'گروه ها',
        'reviews' => 'نظرات',
        'newsletter-subscriptions' => 'اعضای خبرنامه',
        'configure' => 'پیکربندی',
        'settings' => 'تنظیمات',
        'locales' => 'زبان ها',
        'currencies' => 'واحد های پولی',
        'exchange-rates' => 'نرخ تبدیل ارز',
        'inventory-sources' => 'انبارها',
        'channels' => 'کانال ها',
        'users' => 'کابران',
        'roles' => 'نقش ها',
        'sliders' => 'اسلایدر ها',
        'taxes' => 'مالیت ها',
        'tax-categories' => 'دسته بندی های مالیات',
        'tax-rates' => 'نرخ های مالیات'
    ],

    'acl' => [
        'dashboard' => 'داشبورد',
        'sales' => 'سفارشات',
        'orders' => 'سفارشات',
        'shipments' => 'حمل و نقل',
        'invoices' => 'فاکتورها',
        'catalog' => 'کاتالوگ',
        'products' => 'محصولات',
        'categories' => 'دسته بندی ها',
        'attributes' => 'ویژگی ها',
        'attribute-families' => 'خانواده ویژگی ها',
        'customers' => 'مشتریان',
        'groups' => 'گروه ها',
        'reviews' => 'نظرات',
        'newsletter-subscriptions' => 'اعضای خبرنامه',
        'configure' => 'پیکربندی',
        'settings' => 'تنظیمات',
        'locales' => 'زبان ها',
        'currencies' => 'واحد های پولی',
        'exchange-rates' => 'نرخ تبدیل ارز',
        'inventory-sources' => 'انبارها',
        'channels' => 'کانال ها',
        'users' => 'کاربران',
        'roles' => 'نقش ها',
        'sliders' => 'اسلایدرها',
        'taxes' => 'مالیات ها',
        'tax-categories' => 'دسته بندی های مالیات',
        'tax-rates' => 'نرخ مالیات',
    ],

    'dashboard' => [
        'title' => 'داشبورد',
        'from' => 'از',
        'to' => 'به',
        'total-customers' => 'مجموع مشتریان',
        'total-orders' => 'مجموع سفارش ها',
        'total-sale' => 'مجموع فروش',
        'average-sale' => 'میانگین سفارش',
        'increased' => ':progress%',
        'decreased' => ':progress%',
        'sales' => 'سفارشات',
        'top-performing-categories' => 'برترین دسته بندی ها',
        'product-count' => ':count محصولات',
        'top-selling-products' => 'پرفروش ترین محصولات',
        'sale-count' => ':count سفارش',
        'customer-with-most-sales' => 'مشتریان با بیشترین خرید',
        'order-count' => ':count سفارشات',
        'revenue' => 'سود :total',
        'stock-threshold' => 'Stock Threshold',
        'qty-left' => ':qty باقیمانده',
    ],

    'datagrid' => [
        'mass-ops' => [
            'method-error' => 'متد اشتباهی انتخاب شده است.لطفا تنظیمات را بررسی کنید',
            'delete-success' => 'تعداد شاخص انتخاب شده :resource با موفقیت حذف گردید',
            'partial-action' => 'بعضی از اکشن ها به خاطر محدودیت های ایجاد شده توسط سیستم قابل انجام نیست :resource',
            'update-success' => 'تعداد شاخاص انتخاب شده :resource با موفقیت بروزرسانی شد',
        ],

        'id' => 'شناسه',
        'status' => 'وضعیت',
        'code' => 'کد',
        'admin-name' => 'نام',
        'name' => 'نام',
        'fullname' => 'نام خانوادگی',
        'type' => 'نوع',
        'required' => 'ضروری',
        'unique' => 'یکتا',
        'per-locale' => 'براساس زبان',
        'per-channel' => 'بر اساس کانال',
        'position' => 'موقعیت',
        'locale' => 'زبان',
        'hostname' => 'Hostname',
        'email' => 'ایمیل',
        'group' => 'گروه',
        'title' => 'هنوان',
        'comment' => 'نظر',
        'product-name' => 'محصول',
        'currency-name' => 'نام واحد مالی',
        'exch-rate' => 'نرخ تبدیل',
        'priority' => 'اولویت',
        'subscribed' => 'عضو شده',
        'base-total' => 'مجموع',
        'grand-total' => 'مجموع نهایی',
        'order-date' => 'تاریخ سفارش',
        'channel-name' => 'نام کانال',
        'billed-to' => 'فاکتور شده برای',
        'shipped-to' => 'حمل و نقل شده برای',
        'order-id' => 'شناسه سفارش',
        'invoice-date' => 'تاریخ صورت حساب',
        'total-qty' => 'مجموع موجودی',
        'inventory-source' => 'انبار',
        'shipment-date' => 'تاریخ حمل و نقل',
        'shipment-to' => 'ارسال به',
        'sku' => 'کد محصول',
        'price' => 'قیمت',
        'qty' => 'مقدار',
        'permission-type' => 'نوع دسترسی',
        'identifier' => 'شاخص',
        'state' => 'محله',
        'country' => 'کشور',
        'tax-rate' => 'نرخ',
        'role' => 'نقش',
        'sub-total' => 'مجموع فرعی',
        'no-of-products' => 'تعداد محصولات',
        'attribute-family' => 'خانواده ویژگی'
    ],

    'account' => [
        'title' => 'حساب کاربری من',
        'save-btn-title' => 'ذخیره',
        'general' => 'عمومی',
        'name' => 'نام',
        'email' => 'ایمیل',
        'password' => 'رمز عبور',
        'confirm-password' => 'تایید رمز عبور',
        'change-password' => 'تغییر رمز عبور حساب کاربری',
        'current-password' => 'رمز عبور فعلی'
    ],

    'users' => [
        'forget-password' => [
            'title' => 'فراموشی رمز عبور',
            'header-title' => 'بازیابی رمز عبور',
            'email' => 'ایمیل ثبت شده',
            'password' => 'رمز عبور',
            'confirm-password' => 'تایید رمز عبور',
            'back-link-title' => 'بازگشت به وردو به حساب کاربری',
            'submit-btn-title' => 'لینک بازیابی رمز عبور'
        ],

        'reset-password' => [
            'title' => 'بازیابی رمز عبور',
            'title' => 'بازیابی رمز عبور',
            'email' => 'ایمیل ثبت شده',
            'password' => 'رمز عبور',
            'confirm-password' => 'تایید رمز عبور',
            'back-link-title' => 'بازگشت به ورود به حساب کاربری',
            'submit-btn-title' => 'بازیابی رمز عبور'
        ],

        'roles' => [
            'title' => 'نقش ها',
            'add-role-title' => 'اضافه کردن نقش جدید',
            'edit-role-title' => 'ویرایش نقش',
            'save-btn-title' => 'ذخیره نقش',
            'general' => 'عمومی',
            'name' => 'نام',
            'description' => 'توضیحات',
            'access-control' => 'کنترل دسترسی',
            'permissions' => 'حق دسترسی ها',
            'custom' => 'سفارشی',
            'all' => 'همه'
        ],

        'users' => [
            'title' => 'کاربر',
            'add-user-title' => 'اضافه کردن کاربر جدید',
            'edit-user-title' => 'ویرایش کاربر',
            'save-btn-title' => 'ذخیره کردن کاربر',
            'general' => 'عمومی',
            'email' => 'ایمیل',
            'name' => 'نام',
            'password' => 'رمز عبور',
            'confirm-password' => 'تایید رمز عبور',
            'status-and-role' => 'وضعیت و نقش',
            'role' => 'نقش',
            'status' => 'وضعیت',
            'account-is-active' => 'حساب کاربری فعال است',
            'current-password' => 'رم عبور فعلی را وارد کنید',
            'confirm-delete' => 'حذف این حساب کاربری را تایید کنید',
            'confirm-delete-title' => 'قبل از حذف رمز عبور را مجددا وارد نمایید',
            'delete-last' => 'حداقل یک کاربر با نقش مدیر مورد نیاز است',
            'delete-success' => 'کاربر با موفقیت حذف شد',
            'incorrect-password' => 'رمز عبوری که وارد کرده اید اشتباه است',
            'password-match' => 'رمز عبور فعلی با قبلی مغایرت دارد',
            'account-save' => 'تغییرا حساب کاربری با موفقیت ذخیره شد',
            'login-error' => 'لطفا مقادیر وارد شده را مجددا بررسی کنید و مجددا امتحان کنید',
            'activate-warning' => 'اکانت شما هنوز باید تایید شود لطفا با مدیر تماس بگیرید'
        ],

        'sessions' => [
            'title' => 'ورود',
            'email' => 'ایمیل',
            'password' => 'رمز عبور',
            'forget-password-link-title' => 'رمز عبور را فراموش کرده اید؟',
            'remember-me' => 'مرا به خاطر داشته باش',
            'submit-btn-title' => 'ورود به حساب کاربری'
        ]
    ],

    'sales' => [
        'orders' => [
            'title' => 'سفارشات',
            'view-title' => 'سفارش شماره #:order_id',
            'cancel-btn-title' => 'لغو',
            'shipment-btn-title' => 'حمل و نقل',
            'invoice-btn-title' => 'فاکتور',
            'info' => 'اطلاعات',
            'invoices' => 'فاکتورها',
            'shipments' => 'حمل و نقل',
            'order-and-account' => 'سفارش و حساب کاربری',
            'order-info' => 'اطلاعات سفارش',
            'order-date' => 'تاریخ سفارش',
            'order-status' => 'وضعیت سفارش',
            'channel' => 'لغو',
            'customer-name' => 'نام مشتری',
            'email' => 'ایمیل',
            'contact-number' => 'شماره تماس',
            'account-info' => 'اطلاعات حساب کاربری',
            'address' => 'آدرس',
            'shipping-address' => 'آدرس حمل و نقل',
            'billing-address' => 'آدرس صورت حساب',
            'payment-and-shipping' => 'پرداخت و حمل و نقل',
            'payment-info' => 'اطلاعات پرداخت',
            'payment-method' => 'روش پرداخت',
            'currency' => 'واحد پولی',
            'shipping-info' => 'اطلاعات حمل و نقل',
            'shipping-method' => 'روش حمل و نقل',
            'shipping-price' => 'هزینه حمل و نقل',
            'products-ordered' => 'محصولات سفارش داده شده',
            'SKU' => 'کد محصول',
            'product-name' => 'نام محصول',
            'qty' => 'مقدار',
            'item-status' => 'وضعیت آیتم',
            'item-ordered' => 'سفارش داده شده (:qty_ordered)',
            'item-invoice' => 'فاکتور شده (:qty_invoiced)',
            'item-shipped' => 'حمل و نقل شده (:qty_shipped)',
            'item-canceled' => 'لغو شده (:qty_canceled)',
            'price' => 'قیمت',
            'total' => 'مجموع',
            'subtotal' => 'مجموع فرعی',
            'shipping-handling' => 'حمل و نقل و مدیریت',
            'tax' => 'مالیات',
            'tax-percent' => 'درصد مالیات',
            'tax-amount' => 'مقدار مالیات',
            'discount-amount' => 'مقدار تخفیف',
            'grand-total' => 'مجموع نهایی',
            'total-paid' => 'مجموع پرداخت شده',
            'total-refunded' => 'مجموع بازگشت داده شده',
            'total-due' => 'Total Due',
            'cancel-confirm-msg' => 'آیا مطمئنید که می خواهید این سفارش را لغو کنید؟'
        ],

        'invoices' => [
            'title' => 'فاکتورها',
            'id' => 'شناسه',
            'invoice-id' => 'شناسه فاکتور',
            'date' => 'تاریخ فاکتور',
            'order-id' => 'شناسه سفارش',
            'customer-name' => 'نام مشتری',
            'status' => 'وضعیت',
            'amount' => 'مقدار',
            'action' => 'فعالیت',
            'add-title' => 'ایجاد فاکتور',
            'save-btn-title' => 'ذخیره فاکتور',
            'qty' => 'مقدار',
            'qty-ordered' => 'مقدار سفارش داده شده',
            'qty-to-invoice' => 'مقدار فاکتور شده',
            'view-title' => 'فاکتور #:invoice_id',
            'bill-to' => 'فاکتور شده برای',
            'ship-to' => 'حمل و نقل شده برای',
            'print' => 'چاپ',
            'order-date' => 'تاریخ سفارش',
            'creation-error' => 'ایجاد فاکتور برای سفارش امکان پذیر نیست',
            'product-error' => 'فاکتور بدون محصولات نمی تواند ایجاد شود'
        ],

        'shipments' => [
            'title' => 'روش های حمل و نقل',
            'id' => 'شناسه',
            'date' => 'تاریخ حمل و نقل',
            'order-id' => 'شناسه سفارش',
            'order-date' => 'تاریخ سفارش',
            'customer-name' => 'نام مشتری',
            'total-qty' => 'مقدار کلی',
            'action' => 'فعالیت',
            'add-title' => 'ایجاد روش حمل و نقل جدید',
            'save-btn-title' => 'ذخیره روش حمل و نقل',
            'qty-ordered' => 'مقدار سفارش داده شده',
            'qty-to-ship' => 'مقداری که باید حمل و نقل شود',
            'available-sources' => 'منابع موجود',
            'source' => 'منبع',
            'select-source' => 'لطفا منبع را انتخاب کنید',
            'qty-available' => 'مقدار در دسترس',
            'inventory-source' => 'انبار منبع',
            'carrier-title' => 'نام حامل',
            'tracking-number' => 'شماره پیگیری',
            'view-title' => 'روش حمل و نقل #:shipment_id',
            'creation-error' => 'حمل و نقل نمی تواند برای این سفارش ایجاد شود',
            'order-error' => 'ایجاد روش حمل و نقل برای سفارش اجازه داده نشده است',
            'quantity-invalid' => 'مقدار درخواست داده شده معتبر نیست یا موجود نیست',
        ]
    ],

    'catalog' => [
        'products' => [
            'title' => 'محصولات',
            'add-product-btn-title' => 'اضافه کردن محصول جدید',
            'add-title' => 'اضافه کردن محصول جدید',
            'edit-title' => 'ویرایش محصول',
            'save-btn-title' => 'ذخیره محصول',
            'general' => 'عمومی',
            'product-type' => 'نوع محصول',
            'simple' => 'ساده',
            'configurable' => 'متغیر',
            'familiy' => 'خانواده ویژگی',
            'sku' => 'کد محصول',
            'configurable-attributes' => 'ویژگی های متغیر',
            'attribute-header' => 'ویژگی ها',
            'attribute-option-header' => 'گزینه های ویژگی ها',
            'no' => 'نه',
            'yes' => 'بله',
            'disabled' => 'غیر فعال',
            'enabled' => 'فعال',
            'add-variant-btn-title' => 'اضافه کردن متغیر',
            'name' => 'نام',
            'qty' => 'مقدار',
            'price' => 'قیمت',
            'weight' => 'وزن',
            'status' => 'وضعیت',
            'enabled' => 'فعال',
            'disabled' => 'غیرفعال',
            'add-variant-title' => 'اضافه کردن متغیر',
            'variant-already-exist-message' => 'متغیر در حال حاضر وجود دارد',
            'add-image-btn-title' => 'اضافه کردن عکس جدید',
            'mass-delete-success' => 'تمامی محصولات انتخاب شده با موفقیت حذف گردید',
            'mass-update-success' => 'تمامی محصولات انتخاب شده با موفقیت بروزرسانی گردید',
            'configurable-error' => 'لطفا حداقل یک ویژگی متغیر تعریف کنید',
            'categories' => 'دسته بندی ها',
            'images' => 'تصاویر',
            'inventories' => 'انبارها',
            'variations' => 'متغیرها',
            'product-link' => 'لینک محصول',
            'cross-selling' => 'فروش مکمل',
            'up-selling' => 'بیش فروشی',
            'related-products' => 'محصولات مرتبط',
            'product-search-hint' => 'نام محصول را وارد کنید',
            'no-result-found' => 'هیچ محصولی یافت نشد',
            'searching' => 'در حال جستجو ...'
        ],

        'attributes' => [
            'title' => 'ویژگی ها',
            'add-title' => 'اضافه کردن ویژگی جدید',
            'edit-title' => 'ویرایش ویژگی',
            'save-btn-title' => 'ذخیره ویژگی',
            'general' => 'عمومی',
            'code' => 'کد ویژگی',
            'type' => 'نوع ویژگی',
            'text' => 'متن',
            'textarea' => 'فضای متن',
            'price' => 'قیمت',
            'boolean' => 'درست یا غلط',
            'select' => 'انتخاب',
            'multiselect' => 'چند انتخابی',
            'datetime' => 'تاریخ یونیکسی',
            'date' => 'تاریخ',
            'label' => 'لیبل',
            'admin' => 'مدیر',
            'options' => 'ویژگی ها',
            'position' => 'موقعیت',
            'add-option-btn-title' => 'اضافه کردن تنظیم جدید',
            'validations' => 'اعتبار سنجی ها',
            'input_validation' => 'اعتبار سنجی فیلد',
            'is_required' => 'ضروری است؟',
            'is_unique' => 'یکتا است؟',
            'number' => 'عدد صحیح',
            'decimal' => 'عدد اعشاری',
            'email' => 'ایمیل',
            'url' => 'آدرس',
            'configuration' => 'پیکربندی',
            'status' => 'وضعیت',
            'yes' => 'بله',
            'no' => 'نه',
            'value_per_locale' => 'قیمت بر اساس زبان',
            'value_per_channel' => 'قیمت بر اساس کانال',
            'value_per_channel' => 'قیمت بر اساس کانال',
            'is_filterable' => 'استفاده از منو',
            'is_configurable' => 'استفاده به عنوان ویژگی برای ساخت محصول متغیر',
            'admin_name' => 'نام مدیر',
            'is_visible_on_front' => 'آیا محصول در صفحه اصلی سایت قابل مشاهده باشد',
            'swatch_type' => 'نوع گزینش',
            'dropdown' => 'لیستی',
            'color-swatch' => 'انتخاب رنگ',
            'image-swatch' => 'انتخاب عکس',
            'text-swatch' => 'انتخاب متن',
            'swatch' => 'انتخاب'
        ],
        'families' => [
            'title' => 'خانواده',
            'add-family-btn-title' => 'اضافه کردن خانواده جدید',
            'add-title' => 'اضافه کردن خانواده جدید',
            'edit-title' => 'ویرایش خانواده',
            'save-btn-title' => 'ذخیره خانواده',
            'general' => 'عمومی',
            'code' => 'کد خانواده',
            'name' => 'نام',
            'groups' => 'گروه ها',
            'add-group-title' => 'اضافه کردن گروه جدید',
            'position' => 'موقعیت',
            'attribute-code' => 'کد',
            'type' => 'نوع',
            'add-attribute-title' => 'اضافه کردن ویژگی',
            'search' => 'جستجو',
            'group-exist-error' => 'گروهی با نام مشابه در حال حاضر وجود دارد'
        ],
        'categories' => [
            'title' => 'دسته بندی ها',
            'add-title' => 'اضافه کردن دسته بندی',
            'edit-title' => 'ویرایش دسته بندی',
            'save-btn-title' => 'ذخیره دسته بندی',
            'general' => 'عمومی',
            'name' => 'نام',
            'visible-in-menu' => 'نمایش در منو',
            'yes' => 'بله',
            'no' => 'نه',
            'position' => 'موقعیت',
            'display-mode' => 'حالت نمایش',
            'products-and-description' => 'محصولات و توضیحات',
            'products-only' => 'فقط محصولات',
            'description-only' => 'فقط توضیحات',
            'description-and-images' => 'توضیحات و عکس ها',
            'description' => 'توضیحات',
            'parent-category' => 'دسته بندی والد',
            'seo' => 'بهینه سازی برای موتور های جستجو',
            'slug' => 'آدرس',
            'meta_title' => 'عنوان متا',
            'meta_description' => 'توضیحات متا',
            'meta_keywords' => 'کلمات کلیدی متا',
            'image' => 'عکس',
        ]
    ],

    'configuration' => [
        'title' => 'پیکربندی',
        'save-btn-title' => 'ذخیره',
        'save-message' => 'پیکربندی با موفقیت ذخیره شد',
        'yes' => 'بله',
        'no' => 'نه',
        'delete' => 'حذف',

        'tax-categories' => [
            'title' => 'دسته بندی های مالیات',
            'add-title' => 'اضافه کردن دسته بندی مالیات',
            'edit-title' => 'ویرایش دسته بندی مالیات',
            'save-btn-title' => 'ذخیره دسته بندی مالیات',
            'general' => 'دسته بندی مالیات',
            'select-channel' => 'انتخاب کانال',
            'name' => 'نام',
            'code' => 'کد',
            'description' => 'توضیحات',
            'select-taxrates' => 'انتخاب نرخ مالیات',
            'edit' => [
                'title' => 'ویرایش دسته بندی مالیات',
                'edit-button-title' => 'ویرایش دسته بندی مالیات'
            ]
        ],

        'tax-rates' => [
            'title' => 'نرخ های مالیات',
            'add-title' => 'اضافه کردن نرخ مالیات جدید',
            'edit-title' => 'ویرایش نرخ مالیات',
            'save-btn-title' => 'ذخیره نرخ مالیات',
            'general' => 'نرخ مالیات',
            'identifier' => 'مشخص کننده',
            'is_zip' => 'فعال کردن رنج کد پستی',
            'zip_from' => 'از کد پستی',
            'zip_to' => 'تا کد پستی',
            'state' => 'محله',
            'select-state' => 'یک شهر یا محله را انتخاب کنید',
            'country' => 'کشور',
            'tax_rate' => 'نرخ',
            'edit' => [
                'title' => 'ویرایش نرخ مالیات',
                'edit-button-title' => 'ویرایش نرخ'
            ],
            'zip_code' => 'کد پستی',
            'is_zip' => 'فعال کردن رنج کد پستی',
        ],

        'sales' => [
            'shipping-method' => [
                'title' => 'روش های حمل و نقل',
                'save-btn-title' => 'ذخیره',
                'description' => 'توضیحات',
                'active' => 'فعال',
                'status' => 'وضعیت'
            ]
        ]
    ],

    'settings' => [
        'locales' => [
            'title' => 'زبان ها',
            'add-title' => 'اضافه کردن زبان جدید',
            'edit-title' => 'ویرایش زبان',
            'add-title' => 'اضافه کردن زبان جدید',
            'save-btn-title' => 'ذخیره زبان',
            'general' => 'عمومی',
            'code' => 'کد',
            'name' => 'نام'
        ],
        'countries' => [
            'title' => 'کشورها',
            'add-title' => 'اضافه کردن کشور جدید',
            'save-btn-title' => 'ذخیره کشور',
            'general' => 'عمومی',
            'code' => 'کد',
            'name' => 'نام'
        ],
        'currencies' => [
            'title' => 'واحد های پولی',
            'add-title' => 'اضافه کردن واحد پولی جدید',
            'edit-title' => 'ویرایش واحد پولی',
            'save-btn-title' => 'ذخیره واحد پولی',
            'general' => 'عمومی',
            'code' => 'کد',
            'name' => 'نام',
            'symbol' => 'سمبل'
        ],
        'exchange_rates' => [
            'title' => 'نرخ تبدیل ارز',
            'add-title' => 'اضافه کردن نرخ تبدیل ارز جدید',
            'edit-title' => 'ویرایش نرخ تبدیل ارز',
            'save-btn-title' => 'ویرایش ترخ تبدیل ارز',
            'general' => 'عمومی',
            'source_currency' => 'کشور مبدا',
            'target_currency' => 'کشور مقصد',
            'rate' => 'نرخ'
        ],
        'inventory_sources' => [
            'title' => 'انبارها',
            'add-title' => 'اضافه کردن انبار جدید',
            'edit-title' => 'ویرایش انبار',
            'save-btn-title' => 'ذخیره انبار',
            'general' => 'عمومی',
            'code' => 'کد',
            'name' => 'نام',
            'description' => 'توضیحات',
            'source-is-active' => 'منبع فعال است',
            'contact-info' => 'اطلاعات تماس',
            'contact_name' => 'نام',
            'contact_email' => 'ایمیل',
            'contact_number' => 'شماره تماس',
            'contact_fax' => 'فکس',
            'address' => 'آدرس مبدا',
            'country' => 'کشور',
            'state' => 'محله',
            'city' => 'شهر',
            'street' => 'خیابان',
            'postcode' => 'کد پستی',
            'priority' => 'اولویت',
            'latitude' => 'عرض جغرافیایی',
            'longitude' => 'طول جغرافیایی',
            'status' => 'وضعیت'
        ],
        'channels' => [
            'title' => 'کانال ها',
            'add-title' => 'اضافه کردن کانال جدید',
            'edit-title' => 'ویرایش کانال',
            'save-btn-title' => 'ذخیره کانال',
            'general' => 'عمومی',
            'code' => 'کد',
            'name' => 'نام',
            'description' => 'توضیحات',
            'hostname' => 'Hostname',
            'currencies-and-locales' => 'واحد های پولی و زبانی',
            'locales' => 'زبان ها',
            'default-locale' => 'زبان پیش فرض',
            'currencies' => 'واحد های پولی',
            'base-currency' => 'واحد مالی پایه',
            'root-category' => 'دسته بندی ریشه',
            'inventory_sources' => 'انبار',
            'design' => 'طراحی',
            'theme' => 'قالب',
            'home_page_content' => 'محتوای صفحه اصلی',
            'footer_content' => 'محتوای فوتر',
            'logo' => 'لوگو',
            'favicon' => 'فاو آیکون'
        ],

        'sliders' => [
            'title' => 'اسلایدرها',
            'add-title' => 'اضافه کردن اسلایدر جدید',
            'edit-title' => 'ویرایش اسلایدر',
            'save-btn-title' => 'ذخیره اسلایدر',
            'general' => 'عمومی',
            'image' => 'عکس',
            'content' => 'محتوا',
            'channels' => 'کانال',
            'created-success' => 'اسلایدر با موفقیت ایجاد شد',
            'created-fault' => 'خطا در ایجاد اسلایدر',
            'update-success' => 'اسلایدر با موفقیت بروزرسانی شد',
            'update-fail' => 'اسلایدر نمی تواند بروزرسانی شود',
            'delete-success' => 'نمیتوان آخرین آیتم اسلایدر را حذف کرد',
            'delete-fail' => 'آیتم اسلایدر با موفقیت حذف شد'
        ],

        'tax-categories' => [
            'title' => 'ایجاد دسته بندی مالیات',
            'add-title' => 'اضافه کردن دستبه بندی مالیات جدید',
            'edit-title' => 'ویرایش دسته بندی مالیات',
            'save-btn-title' => 'ذخیره دسته بندی مالیات',
            'general' => 'دسته بندی مالیات',
            'select-channel' => 'انتخاب کانال',
            'name' => 'نام',
            'code' => 'کد',
            'description' => 'توضیحات',
            'select-taxrates' => 'انتخاب نرخ مالیات',
            'edit' => [
                'title' => 'ویرایش دسته بندی مالیات',
                'edit-button-title' => 'ویرایش دسته بندی مالیات'
            ],
            'create-success' => 'دسته بندی مالیات جدید ایجاد شد',
            'create-error' => 'در هنگام ایجاد دسته بندی جدید مشکلی رخ داد',
            'update-success' => 'دستبه بندی مالیات با موفقیت بروزرسانی شد',
            'update-error' => 'در هنگام بروزرسانی دسته بندی مالیات مشکی رخ داد',
            'atleast-one' => 'شما نمی توانید آخرین دسته بندی مالیات را حذف کنید',
            'delete' => 'دسته بندی مالیات با موفقیت حذف گردید'
        ],

        'tax-rates' => [
            'title' => 'نرخ مالیات',
            'add-title' => 'ایجاد نرخ مالیات',
            'edit-title' => 'ویرایش نرخ مالیات',
            'save-btn-title' => 'ذخیره نرخ مالیات',
            'general' => 'نرخ مالیات',
            'identifier' => 'شاخص',
            'is_zip' => 'فعال کردن رنج کد پستی',
            'zip_from' => 'از کد پستی',
            'zip_to' => 'تا کد پستی',
            'state' => 'محله',
            'select-state' => 'یک شهر یا محله را انتخاب کنید',
            'country' => 'کشور',
            'tax_rate' => 'نرخ مالیات',
            'edit' => [
                'title' => 'ویرایش نرخ مالیات',
                'edit-button-title' => 'ویرایش نرخ'
            ],
            'zip_code' => 'کد پستی',
            'is_zip' => 'فعال کردن رنج کد پستی',
            'create-success' => 'نرخ مالیات با موفقیت ایجاد شد',
            'create-error' => 'نمی توان نرخ مالیات جدید ایجاد کرد',
            'update-success' => 'نرخ مالیات با موفقیت بروزرسانی شد',
            'update-error' => 'نرخ مالیات نمی تواند آپدیت شود',
            'delete' => 'نرخ مالیات با موفقیت حذف گردید',
            'atleast-one' => 'نمی توان آخرین نرخ مالیات را حذف کرد'
        ]
    ],

    'customers' => [
        'groups' =>[
            'add-title' => 'اضافه کردن گروه جدید',
            'edit-title' => 'ویرایش گروه',
            'save-btn-title' => 'ذخیره گروه',
            'title' => 'گروه ها',
            'save-btn-title' => 'ذخیره گروه',
            'name' => 'نام',
            'is_user_defined' => 'کاربر تعریف شده',
            'yes' => 'بله'
        ],
        'customers' => [
            'add-title' => 'اضافه کردن مشتری جدید',
            'edit-title' => 'ویرایش مشتری',
            'title' => 'مشتریان',
            'first_name' => 'نام',
            'last_name' => 'نام خانوادگی',
            'gender' => 'جنسیت',
            'email' => 'ایمیل',
            'date_of_birth' => 'تاریخ تولد',
            'phone' => 'موبایل',
            'customer_group' => 'گروه مشتری',
            'save-btn-title' => 'ذخیره مشتری',
            'channel_name' => 'نام کانال',
            'state' => 'محله',
            'select-state' => 'یک شهر یا محله را انتخاب کنید',
            'country' => 'کشور',
            'male' => 'مرد',
            'female' => 'زن',
            'phone' => 'موبایل',
            'group-default' => 'نمی توان گروه پیش فرض را حذف کرد',
        ],
        'reviews' => [
            'title' => 'نظرات',
            'edit-title' => 'ویرایش نظر',
            'rating' => 'امتیازات',
            'status' => 'وضعیت ها',
            'comment' => 'نظر',
            'pending' => 'انتظار برای تایید',
            'approved' => 'تایید',
            'disapproved' => 'رد شده'
        ],

        'subscribers' => [
            'title' => 'اعضای خبرنامه',
            'title-edit' => 'ویرایش عضو خبرنامه',
            'email' => 'ایمیل',
            'is_subscribed' => 'عضو',
            'edit-btn-title' => 'ویرایش عضو',
            'update-success' => 'عضو با موفقیت ویرایش گردید',
            'update-failed' => 'شما نمی توانید این عضو را از عضویت خارج کنید',
            'delete' => 'عضو با موفقیت حذف گردید',
            'delete-failed' => 'عضو خبرنامه نمی تواند حذف شود'
        ]
    ],

    'error' => [
        'go-to-home' => 'رفتن به صفحه اصلی',

        '404' => [
            'page-title' => '404 Page not found',
            'name' => '404',
            'title' => 'آدرسی مطابق با درخواست شما پیدا نشد',
            'message' => 'آدرسی که شما دنبال آن می گردید وجود ندارد یا منتقل شده است. لطفا از منوی بالای سایت استفاده نمایید'
        ],
        '403' => [
            'page-title' => '403 forbidden Error',
            'name' => '403',
            'title' => 'منطقه ممنوعه',
            'message' => 'شما حق دسترسی لازم برای ورود به این صفحه را ندارید'
        ],
        '500' => [
            'page-title' => '500 Internal Server Error',
            'name' => '500',
            'title' => 'در سرور مشکلی رخ داده است',
            'message' => 'در سرور وب سایت مشکلی رخ داده است. ما در حال رفع این مشکل هستیم'
        ],
        '401' => [
            'page-title' => '401 Unauthorized Error',
            'name' => '401',
            'title' => 'مشکل حق دسترسی',
            'message' => 'شما برای دسترسی به لینک مورد نظر حق دسترسی لازم را ندارید'
        ],
    ],

    'export' => [
        'export' => 'گرفتن خروجی',
        'import' => 'وراد کردن گروهی',
        'format' => 'انتخاب فرمت',
        'download' => 'دانلود',
        'upload' => 'آپلود',
        'csv' => 'CSV',
        'xls' => 'XLS',
        'file' => 'File',
        'upload-error' => 'فایل باید از انواع روبرو باشد: xls, xlsx, csv.',
        'duplicate-error' => 'شاخص باید منحصر به فرد باشد :شاخص در ردیف :position.',
        'enough-row-error' => 'فایل تعداد ردیف های لازم را ندارد',
        'allowed-type' => 'انواع فایل مجاز :',
        'file-type' => 'csv, xls, xlsx.',
        'no-records' => 'چیزی برای خروجی گرفتن وجود ندارد',
        'illegal-format' => 'این فرمت نامعتر است یا وجود ندارد'
    ],

    'response' => [
        'create-success' => ':name با موفقیت ایجاد شد',
        'update-success' => ':name با موفقیت بروزرسانی شد',
        'delete-success' => ':name با موفقیت حذف گردید',
        'last-delete-error' => 'حداقل یک :name ضروری است',
        'user-define-error' => 'نمیتوان حذف کرد :name',
        'attribute-error' => ':name در محصولات متغیر استفاده شده است',
        'attribute-product-error' => ':name در محصولات استفاده شده است',
        'customer-associate' => ':name نمی تواند حذف شود زیرا تعدادی مشتری در این گروه قرار دارند',
        'currency-delete-error' => 'این واحد پولی به عنوان پایه واحد پولی یک کانال ثبت شده است بنابراین نمی تواند حذف شود',
        'upload-success' => ':name با موفقیت بروزرسانی گردید',
        'delete-category-root' => 'نمی توان دسته بندی ریشه را حذف کرد',
        'create-root-failure' => 'دسته بندی با این نام هم اکنون نیز وجود دارد',
        'cancel-success' => ':name با موفقیت لغو شد',
        'cancel-error' => ':name نمی تواند لغو شود',
        'already-taken' => ':name از قبل برداشته شده است'
    ],

    'footer' => [
        'copy-right' => 'تمامی حقوق برای مازاریت محفوظ است'
    ],

    'admin' => [
        'system' => [
            'catalog' => 'کاتالوگ',
            'products' => 'محصولات',
            'review' => 'نظر',
            'allow-guest-review' => 'اجازه به کاربر مهمان برای ارسال نظر',
            'customer' => 'مشتری',
            'settings' => 'تنظیمات',
            'address' => 'آدرس',
            'address' => 'آدرس',
            'street-lines' => 'تعداد سطر در آدرس خیابان',
            'sales' => 'سفارشات',
            'shipping-methods' => 'روش های حمل و نقل',
            'free-shipping' => 'حمل و نقل رایگان',
            'flate-rate-shipping' => 'حمل و نقل با نرخ ثابت',
            'shipping' => 'حمل و نقل',
            'origin' => 'مبدا',
            'country' => 'کشور',
            'state' => 'محله',
            'zip' => 'کدپستی',
            'city' => 'شهر',
            'street-address' => 'آدرس خیابان',
            'title' => 'عنوان',
            'description' => 'توضیحات',
            'rate' => 'نرخ',
            'status' => 'وضعیت',
            'type' => 'نوع',
            'payment-methods' => 'روش های پرداخت',
            'cash-on-delivery' => 'پرداخت در محل',
            'money-transfer' => 'کارت به کارت',
            'paypal-standard' => 'پی پال',
            'business-account' => 'حساب کاربری'
        ]
    ]
];