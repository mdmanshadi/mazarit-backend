<?php

return [
    'layouts' => [
        'my-account' => 'حساب کاربری من',
        'profile' => 'پروفایل',
        'address' => 'آدرس',
        'reviews' => 'نظرات',
        'wishlist' => 'علاقه مندی ها',
        'orders' => 'سفارشات',
    ],

    'common' => [
        'error' => 'مشکلی رخ داده است لطفا بعدا مجددا امتحان کنید'
    ],

    'home' => [
        'page-title' => 'فروشگاه',
        'featured-products' => 'محصولات محبوب',
        'new-products' => 'محصولات جدید',
        'verify-email' => 'لطفا آدرس ایمیل خود را تایید کنید',
        'resend-verify-email' => 'ارسال مجدد ایمیل تایید'
    ],

    'header' => [
        'title' => 'حساب کاربری',
        'dropdown-text' => 'مدیریت سبد خرید، سفارش ها و علاقه مندی ها',
        'sign-in' => 'ورود',
        'sign-up' => 'ثبت نام',
        'account' => 'حساب کاربری',
        'cart' => 'سبد خرید',
        'profile' => 'پروفایل',
        'wishlist' => 'علاقه مندی ها',
        'cart' => 'سبد خرید',
        'logout' => 'خروج از حساب کاربری',
        'search-text' => 'جستجوی محصولات'
    ],

    'minicart' => [
        'view-cart' => 'مشاهده سبد خرید',
        'checkout' => 'تسویه حساب',
        'cart' => 'سبد خرید',
        'zero' => '0'
    ],

    'footer' => [
        'subscribe-newsletter' => 'عضویت در خبرنامه',
        'subscribe' => 'عضویت',
        'locale' => 'زبان',
        'currency' => 'واحد پولی',
    ],

    'subscription' => [
        'unsubscribe' => 'لغو عضویت',
        'subscribe' => 'عضویت',
        'subscribed' => 'ایمیل شما با موفقیت ثبت شد',
        'not-subscribed' => 'مشکلی رخ داده است لطفا بعدا امتحان کنید',
        'already' => 'شما هم اکنون نیز در خبرنامه ما عضو هستید',
        'unsubscribed' => 'شما از لیست خبرنامه لغو عضویت کرده اید',
        'already-unsub' => 'شما هم اکنون لغو عضویت کرده اید',
        'not-subscribed' => 'مشکلی رخ داده است لطفا بعدا امتحان کنید'
    ],

    'search' => [
        'no-results' => 'هیچ نتیجه ای پیدا نشد',
        'page-title' => 'جستجو در فروشگاه',
        'found-results' => 'نتایجی پیدا شد',
        'found-result' => 'یک نتیجه پیدا شد'
    ],

    'reviews' => [
        'title' => 'عنوان',
        'add-review-page-title' => 'اضافه کرده نظر',
        'write-review' => 'نوشتن یک نظر',
        'review-title' => 'لطفا برای نظر خود یک عنوان در نظر بگیرید',
        'product-review-page-title' => 'نظرسنجی محصول',
        'rating-reviews' => 'امتیازات و نظرها',
        'submit' => 'ثبت',
        'delete-all' => 'تمامی نظرها با موفقیت حذف گردید',
        'ratingreviews' => ':rating امتیاز & :review نظر',
        'star' => 'ستاره',
        'percentage' => ':percentage %',
        'id-star' => 'ستاره',
        'name' => 'نام'
    ],

    'customer' => [
        'signup-text' => [
            'account_exists' => 'شما هم اکنون عضو هستید',
            'title' => 'ثبت نام'
        ],

        'signup-form' => [
            'page-title' => 'فرم ثبت نام مشتری',
            'title' => 'ثبت نام',
            'firstname' => 'نام',
            'lastname' => 'نام خانوادگی',
            'email' => 'ایمیل',
            'password' => 'کلمه عبور',
            'confirm_pass' => 'تایید کلمه عبور',
            'button_title' => 'ثبت نام',
            'agree' => 'موافق',
            'terms' => 'قوانین',
            'conditions' => 'مقررات',
            'using' => 'با استفاده از این وب سایت',
            'agreement' => 'موافق',
            'success' => 'حساب کاربری با موفقیت ایجاد گردید.یک ایمیل برای تایید عضویت برای شما ارسال شد',
            'success-verify-email-not-sent' => 'حساب کاربری ایجاد شد اما ایمیل تایید عضویت ارسال نشد',
            'failed' => 'مشکلی رخ داده است لطفا بعدا امتحان کنید',
            'already-verified' => 'حساب کاربری شما تایید شده است یا لطفا در خواست ارسال مجدد ایمیل تایید را نمایید',
            'verification-not-sent' => 'مشکلی در ارسال ایمیل تایید رخ داده است لطفا بعدا امتحان کنید',
            'verification-sent' => 'ایمیل تایید عضویت ارسال گردید',
            'verified' => 'حساب کاربری شما با موفقیت تایید شد.لطفا هم اکنون وارد سایت شوید',
            'verify-failed' => 'ما نمی توانیم حساب کاربری شما را تایید نماییم',
            'dont-have-account' => 'شما با این ایمیل، حساب کاربری در سایت ندارید',
        ],

        'login-text' => [
            'no_account' => 'حساب کاربری ندارید؟',
            'title' => 'ثبت نام',
        ],

        'login-form' => [
            'page-title' => 'ورود مشتری',
            'title' => 'ورود به حساب کاربری',
            'email' => 'ایمیل',
            'password' => 'رمز عبور',
            'forgot_pass' => 'رمز عبورتان را فراموش کرده اید؟',
            'button_title' => 'ورود',
            'remember' => 'مرا به خاطر بسپار',
            'footer' => 'تمامی حقوق برای مازاریت محفوظ می باشد',
            'invalid-creds' => 'لطفا ورودهای خود را مجددا بررسی کنید و سپس تلاش کنید',
            'verify-first' => 'لطفا ابتدا ایمیل خودتان را تایید کنید',
            'resend-verification' => 'مجددا ایمیل تایید را ارسال نمایید'
        ],

        'forgot-password' => [
            'title' => 'بازیابی رمز عبور',
            'email' => 'ایمیل',
            'submit' => 'ثبت',
            'page_title' => 'فرم فراموشی رمز عبور مشتری'
        ],

        'reset-password' => [
            'title' => 'بازیابی رمز عبور',
            'email' => 'ایمیل ثبت شده',
            'password' => 'رمز عبور',
            'confirm-password' => 'تایید رمز عبور',
            'back-link-title' => 'بازگشت به ورود به حساب کاربری',
            'submit-btn-title' => 'بازیابی رمز عبور'
        ],

        'account' => [
            'dashboard' => 'ویرایش پروفایل مشتری',
            'menu' => 'منو',

            'profile' => [
                'index' => [
                    'page-title' => 'پروفایل مشتری',
                    'title' => 'پروفایل',
                    'edit' => 'ویرایش',
                ],

                'edit-success' => 'پروفایل با موفقیت ویرایش شد',
                'edit-fail' => 'پروفایل ویرایش نشد! لطفا بعدا امتحان کنید',
                'unmatch' => 'پسورد قدیمی اشتباه است',

                'fname' => 'نام',
                'lname' => 'نام خانوادگی',
                'gender' => 'جنسیت',
                'dob' => 'تاریخ تولد',
                'phone' => 'موبایل',
                'email' => 'ایمیل',
                'opassword' => 'رمز عبور قدیمی',
                'password' => 'رمز عبور',
                'cpassword' => 'تایید رمز عبور',
                'submit' => 'بروزرسانی پروفایل',

                'edit-profile' => [
                    'title' => 'ویرایش پروفایل',
                    'page-title' => 'فرم ویرایش پروفایل مشتری'
                ]
            ],

            'address' => [
                'index' => [
                    'page-title' => 'آدرس های مشتری',
                    'title' => 'آدرس',
                    'add' => 'اضافه کردن آدرس جدید',
                    'edit' => 'ویرایش',
                    'empty' => 'شما هیچ آدرس ذخیره شده ای ندارید.برای اضافه کردن آدرس روی دکمه زیر کلیک کنید',
                    'create' => 'ایجاد آدرس',
                    'delete' => 'حذف',
                    'make-default' => 'قرار دادن به عنوان پیش فرض',
                    'default' => 'پیش فرض',
                    'contact' => 'تماس',
                    'confirm-delete' =>  'آیا شما واقعا می خواهید این آدرس را حذف نمایید',
                    'default-delete' => 'آدرس پیش فرض نمی تواند حذف شود'
                ],

                'create' => [
                    'page-title' => 'فرم اضافه کردن آدرس جدید مشتری',
                    'title' => 'اضافه کردن آدرس جدید',
                    'street-address' => 'آدرس خیابان',
                    'country' => 'کشور',
                    'state' => 'شهر',
                    'select-state' => 'یک شهر را انتخاب نمایید',
                    'city' => 'شهر',
                    'postcode' => 'کد پستی',
                    'phone' => 'موبایل',
                    'submit' => 'ذخیره آدرس',
                    'success' => 'آدرس با موفقیت اضافه گردید',
                    'error' => 'آدرس نمی تواند اضافه شود'
                ],

                'edit' => [
                    'page-title' => 'ویرایش آدرس مشتری',
                    'title' => 'ویرایش آدرس',
                    'submit' => 'ذخیره آدرس',
                    'success' => 'آدرس با موفقیت ویرایش گردید'
                ],
                'delete' => [
                    'success' => 'آدرس با موفقیت حذف گردید',
                    'failure' => 'آدرس نمی تواند حذف شود'
                ]
            ],

            'order' => [
                'index' => [
                    'page-title' => 'سفارشات مشتری',
                    'title' => 'سفارشات',
                    'order_id' => 'شناسه سفارش',
                    'date' => 'تاریخ',
                    'status' => 'وضعیت',
                    'total' => 'مجموع'
                ],

                'view' => [
                    'page-tile' => 'سفارش #:order_id',
                    'info' => 'اطلاعات',
                    'placed-on' => 'قرار گفته با',
                    'products-ordered' => 'محصولات سفارش داده شده',
                    'invoices' => 'فاکتورها',
                    'shipments' => 'روش های حمل و نقل',
                    'SKU' => 'کد محصول',
                    'product-name' => 'نام محصول',
                    'qty' => 'مقدار',
                    'item-status' => 'وضعیت آیتم',
                    'item-ordered' => 'سفارش داده شده (:qty_ordered)',
                    'item-invoice' => 'فاکتور شده (:qty_invoiced)',
                    'item-shipped' => 'ارسال شده (:qty_shipped)',
                    'item-canceled' => 'کنسل شده (:qty_canceled)',
                    'price' => 'قیمت',
                    'total' => 'مجموع',
                    'subtotal' => 'مجموع فرعی',
                    'shipping-handling' => 'حمل و نقل و مدیریت',
                    'tax' => 'مالیات',
                    'tax-percent' => 'درصد مالیات',
                    'tax-amount' => 'مقدار مالیات',
                    'discount-amount' => 'مقدار تخفیف',
                    'grand-total' => 'مجموع کلی',
                    'total-paid' => 'مجموع پرداخت شده',
                    'total-refunded' => 'مجموع بازگردانده شده',
                    'total-due' => 'Total Due',
                    'shipping-address' => 'آدرس ارسال',
                    'billing-address' => 'آدرس صورتحساب',
                    'shipping-method' => 'روش ارسال',
                    'payment-method' => 'روش پرداخت',
                    'individual-invoice' => 'فاکتور #:invoice_id',
                    'individual-shipment' => 'حمل و نقل #:shipment_id',
                    'print' => 'پرینت',
                    'invoice-id' => 'شناسه فاکتور',
                    'order-id' => 'شناسه سفارش',
                    'order-date' => 'تاریخ سفارش',
                    'bill-to' => 'صورت حساب برای',
                    'ship-to' => 'حمل و نقل برای',
                    'contact' => 'تماس'
                ]
            ],

            'review' => [
                'index' => [
                    'title' => 'نظرات',
                    'page-title' => 'نظرات مشتریان'
                ],

                'view' => [
                    'page-tile' => 'نظر شماره #:id',
                ]
            ]
        ]
    ],

    'products' => [
        'layered-nav-title' => 'سفارش داده شده توسط',
        'price-label' => 'شروع قیمت از',
        'remove-filter-link-title' => 'حذف همه',
        'sort-by' => 'مرتب کردن بر اساس',
        'from-a-z' => 'از الف - ی',
        'from-z-a' => 'از ی - الف',
        'newest-first' => 'جدیدترین',
        'oldest-first' => 'قدیمی ترین',
        'cheapest-first' => 'ارزان ترین',
        'expensive-first' => 'گران ترین',
        'show' => 'نمایش',
        'pager-info' => 'نمایش :تعداد از :total Items',
        'description' => 'توضیحات',
        'specification' => 'مشخصات',
        'total-reviews' => ':total نظر',
        'total-rating' => ':total_rating امتیاز و :total_reviews نظر',
        'by' => 'براساس :name',
        'up-sell-title' => 'محصولات دیگری که ممکن شما علاقه مند باشید',
        'related-product-title' => 'محصولات مشابه',
        'cross-sell-title' => 'گزینه های بیشتر',
        'reviews-title' => 'نظرات و امتیازان',
        'write-review-btn' => 'نوشتن نظر',
        'choose-option' => 'انتخاب یک گزینه',
        'sale' => 'حراج',
        'new' => 'جدید',
        'empty' => 'هیچ محصولی در این دسته بندی وجود ندارد',
        'add-to-cart' => 'افزودن به سبد خرید',
        'buy-now' => 'خرید نهایی',
        'whoops' => 'اوه !',
        'quantity' => 'مقدار',
        'in-stock' => 'موجودی انبار',
        'out-of-stock' => 'نا موجود در انبار',
        'view-all' => 'مشاهده همه',
        'select-above-options' => 'لطفا ابتدا گزینه های بالایی را انتخاب کنید'
    ],

    'wishlist' => [
        'title' => 'علاقه مندی ها',
        'deleteall' => 'حذف همه',
        'moveall' => 'افزودن همه محصولات به سبد خرید',
        'move-to-cart' => 'انتقال به سبد خرید',
        'error' => 'در اضافه کردن محصول به سبد خرید مشکلی رخ داد. لطفا مجددا امتحان کنید',
        'add' => 'محصول با موفقیت به علاقه مندی ها اضافه شد',
        'remove' => 'محصول با موفقیت از علاقه مندی ها حذف شد',
        'moved' => 'محصول با موفقیت به علاقه مندی ها منتقل شد',
        'move-error' => 'محصول نتوانست به علاقه مندی ها اضافه شود. لطفا بدا امتحان کنید',
        'success' => 'محصول با موفقیت به علاقه مندی ها اضافه شد',
        'failure' => 'محصول نتوانست به سبد خرید افزوده شود. لطفا بعدا امتحان کنید',
        'already' => 'محصول هم اکنون در سبد خرید شما وجود دارد',
        'removed' => 'محصول با موفقیت از علاقه مندی های شما حدف شد',
        'remove-fail' => 'محصول نتوانست از علاقه مندی های شما حذف شود. لطفا بعدا امتحان کنید',
        'empty' => 'شما هیچ محصولی در لیست علاقه مندی هایتان ندارید',
        'remove-all-success' => 'تمام محصولات موجود در علاقه مندی های شما حذف گردید',
    ],

    // 'reviews' => [
    //     'empty' => 'You Have Not Reviewed Any Of Product Yet'
    // ]

    'buynow' => [
        'no-options' => 'لطفا قبل از خرید این محصول مشخصات محصول را انتخاب کنید'
    ],


    'checkout' => [
        'cart' => [
            'integrity' => [
                'missing_fields' =>'بعضی از فیلد های ضروری چر نشده ااست',
                'missing_options' =>'گزینه های متغیر بعضی از محصولات تعیین نشده است',
            ],
            'create-error' => 'در هنگام ایجاد سبد خرید مشکلاتی رخ داده است',
            'title' => 'سبد خرید',
            'empty' => 'سبد خرید شما خالی است',
            'update-cart' => 'بروزرسانی سبد خرید',
            'continue-shopping' => 'ادامه دادن به خرید',
            'proceed-to-checkout' => 'رفتن به تسویه حساب',
            'remove' => 'حذف',
            'remove-link' => 'حذف',
            'move-to-wishlist' => 'انتقال به علاقه مند های',
            'move-to-wishlist-success' => 'محصول به علاقه مندی ها اضافه شد',
            'move-to-wishlist-error' => 'نمی توان محصول را به علاقه مندی ها اضافه کرد. لطفا بعدا امتحان کنید',
            'add-config-warning' => 'لطفا قبل از افزودن به سبد خرید گزینه های متغیر محصول را انتخاب کنید',
            'quantity' => [
                'quantity' => 'مقدار',
                'success' => 'محصولات سبد خرید با موفقیت بروزرسانی شد',
                'illegal' => 'مقدار نمی تواند از یک کمتر باشد',
                'inventory_warning' => 'مقداری که شا سفارش داده اید در حال حاضر موجود نیست. لطفا بعدا امتحان کنید',
                'error' => 'محصولات در حال حاضر بروزرسانی نشدند. لطفا بعدا امتحان کنید'
            ],
            'item' => [
                'error_remove' => 'هیچ محصولی برای حذف از سبد خرید وجود ندارد',
                'success' => 'محصول با موفقیت به سبد خرید افزوده شد',
                'success-remove' => 'محصول با موفقیت از سبد خرید حذف شد',
                'error-add' => 'محصول نتوانست به سبد خرید افزوده شود. لطفا بعدا امتحان کنید',
            ],
            'quantity-error' => 'مقدار درخواست شده موجود نمی باشد',
            'cart-subtotal' => 'مجموع سبد خرید',
            'cart-remove-action' => 'آیا شما واقعا می خواهید این کار را انجام دهید؟'
        ],

        'onepage' => [
            'title' => 'تسویه حساب',
            'information' => 'اطلاعات',
            'shipping' => 'ارسال',
            'payment' => 'پرداخت',
            'complete' => 'تکمیل',
            'billing-address' => 'آدرس صورت حساب',
            'sign-in' => 'ورود به حساب کاربری',
            'first-name' => 'نام',
            'last-name' => 'نام خانوادگی',
            'email' => 'ایمیل',
            'address1' => 'آدرس خیابان',
            'city' => 'شهر',
            'state' => 'محله',
            'select-state' => 'یک شهر یا محله را انتخاب کنید',
            'postcode' => 'کدپستی',
            'phone' => 'موبایل',
            'country' => 'کشور',
            'order-summary' => 'سفارش شما در یک نگاه',
            'shipping-address' => 'آدرس ارسال',
            'use_for_shipping' => 'به این آدرس حمل شود',
            'continue' => 'ادامه',
            'shipping-method' => 'انتخاب روش ارسال',
            'payment-methods' => 'انتخاب روش پرداخت',
            'payment-method' => 'روش پرداخت',
            'summary' => 'سفارش شما در یک نگاه',
            'price' => 'قیمت',
            'quantity' => 'مقدار',
            'billing-address' => 'آدرس صورتحساب',
            'shipping-address' => 'آدرس ارسال',
            'contact' => 'تماس',
            'place-order' => 'ثبت نهایی سفارش',
            'new-address' => 'اضافه کردن آدرس جدید',
            'save_as_address' => 'ذخیره به عنوان آدرس'
        ],

        'total' => [
            'order-summary' => 'سفارش شما در یک نگاه',
            'sub-total' => 'محصولات',
            'grand-total' => 'مجموع نهایی',
            'delivery-charges' => 'هزینه تحویل',
            'tax' => 'مالیات',
            'price' => 'قیمت'
        ],

        'success' => [
            'title' => 'سفارش با موفقیت ثبت گردید',
            'thanks' => 'از اطمینان و ثبت سفارش شما ممنونین',
            'order-id-info' => 'شماره پیگیری سفارش شما #:order_id',
            'info' => 'ما اطلاعات سفارش را برای شما ایمیل خواهیم کرد'
        ]
    ],

    'mail' => [
        'order' => [
            'subject' => 'یک سفارش جدید ثبت شد',
            'heading' => 'تایید سفارش',
            'dear' => 'عزیز :customer_name',
            'greeting' => 'از سفارش شما متشکریم :order_id placed در :created_at',
            'summary' => 'سفارش شما در یک نگاه',
            'shipping-address' => 'آدرس ارسال',
            'billing-address' => 'آدرس صورتحساب',
            'contact' => 'تماس',
            'shipping' => 'ارسال',
            'payment' => 'پرداخت',
            'price' => 'قیمت',
            'quantity' => 'مقدار',
            'subtotal' => 'مجموع',
            'shipping-handling' => 'ارسال',
            'tax' => 'مالیات',
            'grand-total' => 'مجموع نهایی',
            'final-summary' => 'بابت اعتماد شما سپاسگذاریم. مراحل ارسال کالا را برای شما ایمیل خواهیم کرد',
            'help' => 'اگر شما هر سوالی داشتید با ایمیل مقابل ارتباط برقرار کنید :support_email',
            'thanks' => 'متشکریم!'
        ],
        'invoice' => [
            'heading' => 'فاکتور شما #:invoice_id برای Order #:order_id',
            'subject' => 'فاکتور برای سفارش #:order_id',
            'summary' => 'خلاصه فاکتور',
        ],
        'shipment' => [
            'heading' => 'ارسال #:shipment_id برای سفارش #:order_id',
            'subject' => 'ارسال برای سفارش شماره #:order_id',
            'summary' => 'خلاصه ارسال',
            'carrier' => 'حمل و نقل',
            'tracking-number' => 'شماره پیگیری'
        ],
        'forget-password' => [
            'dear' => 'عزیز :name',
            'info' => 'شما این ایمیل را به دلیل اینکه یک درخواست بازیابی پسورد ارسال شده است دریافت کرده اید',
            'reset-password' => 'بازیابی رمز عبور',
            'final-summary' => 'اگر شما در خواست تغییر رمز عبور نداشته اید لازم نیست هیج کاری انجام دهید!',
            'thanks' => 'متشکریم'
        ]
    ],

    'webkul' => [
        'copy-right' => 'تمامی حقوق برای مازاریت محفوظ می باشد',
    ],

    'response' => [
        'create-success' => ':name با موفقیت ایجاد شد',
        'update-success' => ':name با موفقیت بروزرسانی شد',
        'delete-success' => ':name با موفقیت حذف گردید',
        'submit-success' => ':name با موفقیت ثبت شد'
    ],
];